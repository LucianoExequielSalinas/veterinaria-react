function Header() {
  return (
    <>
      <h1 className=" text-center text-4xl  md:w-2/3 font-bold mx-auto uppercase text-indigo-300">
        Seguimiento Pacientes
        <span className="text-indigo-600 "> Veterinaria </span>
      </h1>
    </>
  );
}
export default Header;
