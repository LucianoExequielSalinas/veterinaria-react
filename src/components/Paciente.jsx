export const Paciente = ({ paciente, setPaciente, elminarPaciente }) => {
  const handleEliminiarPaciente = () => {
    const rta = confirm("¿ Seguro que deseas elminar este paciente ?");
    if (rta) {
      elminarPaciente(paciente.id);
    }
  };

  return (
    <div className=" mx-3 mb-3 bg-white shadow-md px-5 py-10 rounded-lg">
      <p className="font-bold mb-3 text-gray-700 uppercase">
        Nombre:{" "}
        <span className="font-normal normal-case">{paciente.nombre}</span>
      </p>
      <p className="font-bold mb-3 text-gray-700 uppercase">
        Propietario:{" "}
        <span className="font-normal normal-case">{paciente.propietario}</span>
      </p>
      <p className="font-bold mb-3 text-gray-700 uppercase">
        Email: <span className="font-normal normal-case">{paciente.email}</span>
      </p>
      <p className="font-bold mb-3 text-gray-700 uppercase">
        Fecha Alta:{" "}
        <span className="font-normal normal-case">{paciente.fecha}</span>
      </p>
      <p className="font-bold mb-3 text-gray-700 uppercase">
        sintomas:{" "}
        <span className="font-normal normal-case">{paciente.sintomas}</span>
      </p>

      <div className=" mt-10 flex justify-between">
        <button
          className=" py-2 px-10 bg-indigo-600 hover:bg-violet-700 text-white font-bold uppercase rounded-md"
          type="button"
          onClick={() => setPaciente(paciente)}
        >
          Editar
        </button>
        <button
          className="py-2 px-10 bg-red-600 hover:bg-red-700 text-white font-bold uppercase rounded-md"
          type="button"
          onClick={handleEliminiarPaciente}
        >
          Eliminar
        </button>
      </div>
    </div>
  );
};
