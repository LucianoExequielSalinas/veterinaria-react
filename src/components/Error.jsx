export const Error = ({ children }) => (
  <div className=" bg-red-400 text-center py-3 uppercase font-bold mb-3 rounded-md">
    <p>{children}</p>
  </div>
);
