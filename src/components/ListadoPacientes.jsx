import React from "react";
import { Paciente } from "./Paciente";

export const ListadoPacientes = ({
  pacientes,
  setUnPaciente,
  elminarPaciente,
}) => {
  return (
    <div className="md:w-1/2 lg:w-3/5 ">
      {pacientes.length != 0 ? (
        <>
          <h2 className="font-black text-3xl text-center text-white">
            Listado Pacientes
          </h2>
          <p className="text-lg mt-4 text-center text-white">
            Adminstra tus
            <span className="text-indigo-500 font-bold">
              {" "}
              Pacientes y citas{" "}
            </span>
          </p>

          <div className=" mt-3 h-screen overflow-y-scroll">
            {pacientes.map((unPaciente) => (
              <Paciente
                key={unPaciente.id}
                paciente={unPaciente}
                setPaciente={setUnPaciente}
                elminarPaciente={elminarPaciente}
              />
            ))}
          </div>
        </>
      ) : (
        <>
          <h2 className="font-black text-3xl text-center text-white">
            No hay pacientes registrados
          </h2>
          <p className="text-lg mt-4 text-center text-white">
            Comienza agregando tus pacientes y {""}
            <span className="text-indigo-500 font-bold">
              {" "}
              Aparecearan Aqui{" "}
            </span>
          </p>
        </>
      )}
    </div>
  );
};
