import { useState, useEffect } from "react";
import { Error } from "./Error";
export const Formulario = ({ setPacientes, pacientes, unPaciente ,setUnPaciente}) => {
  const [nombre, setNombre] = useState("");
  const [propietario, setPropietario] = useState("");
  const [email, setEmail] = useState("");
  const [fecha, setFecha] = useState("");
  const [sintomas, setSintomas] = useState("");
  const [error, setError] = useState(false);

  useEffect(() => {
    if (Object.keys(unPaciente).length > 0) {
      setNombre(unPaciente.nombre);
      setPropietario(unPaciente.propietario);
      setEmail(unPaciente.email);
      setFecha(unPaciente.fecha);
      setSintomas(unPaciente.sintomas);
    }
  }, [unPaciente]);

  const handleSubmit = (e) => {
    e.preventDefault();
    if ([nombre, propietario, email, fecha, sintomas].includes("")) {
      setError(true);
      return;
    }
    setError(false);

    const objetoPaciente = {
      nombre,
      propietario,
      email,
      fecha,
      sintomas,
    };

    if (unPaciente.id) {
      // EDITANDo
      objetoPaciente.id = unPaciente.id;
      console.log( "objetoPaciente", objetoPaciente)
      const pacientesActualizados = pacientes.map((pacienteState) =>
        pacienteState.id === unPaciente.id ? objetoPaciente : pacienteState
      );
      console.log("pacientesActualizados" ,pacientesActualizados)
      setPacientes(pacientesActualizados);
      setUnPaciente({})
    } else {
      //AGREGANDO
      objetoPaciente.id = generarId();
      setPacientes([...pacientes, objetoPaciente]);
    }

    clearForm();
  };

  const generarId = () =>
    Math.random().toString(36).substring(2) + Date.now().toString(36);

  const clearForm = () => {
    setNombre("");
    setPropietario("");
    setEmail("");
    setFecha("");
    setSintomas("");
  };

  return (
    <div className="md:w-1/2  lg:w-2/5">
      <h2 className="font-black text-3xl text-center text-white">
        Seguimiento Pacientes
      </h2>
      <p className="text-lg mt-4 text-center text-white">
        Añade Pacientes y
        <span className="text-indigo-500 font-bold"> Administralos </span>
      </p>
      <form
        action=""
        onSubmit={handleSubmit}
        className="bg-white shadow-sm shadow-indigo-400 rounded-md px-5 py-3 my-3 h-screen "
      >
        {error && <Error>Todos los campos son obligatorios</Error>}
        <div className="my-3">
          <label
            htmlFor="nombre"
            className="block text-gray-700 uppercase font-bold"
          >
            Nombre Mascota
          </label>
          <input
            id="nombre"
            className="border-2 w-full p-2 mt-2 placeholder-slate-600 rounded-md"
            type="text"
            placeholder="Nombre de la mascota"
            value={nombre}
            onChange={(e) => setNombre(e.target.value)}
          />
        </div>
        <div className="my-3">
          <label
            htmlFor="propietario"
            className="block text-gray-700 uppercase font-bold"
          >
            Nombre Propietario
          </label>
          <input
            id="propietario"
            className="border-2 w-full p-2 mt-2 placeholder-slate-600 rounded-md"
            type="text"
            placeholder="Nombre del propietario"
            value={propietario}
            onChange={(e) => setPropietario(e.target.value)}
          />
        </div>
        <div className="my-3">
          <label
            htmlFor="email"
            className="block text-gray-700 uppercase font-bold"
          >
            Email
          </label>
          <input
            id="email"
            className="border-2 w-full p-2 mt-2 placeholder-slate-600 rounded-md"
            type="email"
            placeholder="Email de contacto"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className="my-3">
          <label
            htmlFor="fechaAlta"
            className="block text-gray-700 uppercase font-bold"
          >
            Fecha de alta
          </label>
          <input
            id="fechaAlta"
            className="border-2 w-full p-2 mt-2 placeholder-slate-600 rounded-md"
            type="date"
            value={fecha}
            onChange={(e) => setFecha(e.target.value)}
          />
        </div>
        <div className="my-3">
          <label
            htmlFor="sintomas"
            className="block text-gray-700 uppercase font-bold"
          >
            Sintomas
          </label>
          <textarea
            className="border-2 w-full p-2 mt-2 placeholder-slate-600 rounded-md"
            id="sintomas"
            placeholder="Describe los sintomas"
            value={sintomas}
            onChange={(e) => setSintomas(e.target.value)}
          ></textarea>
        </div>

        <input
          type="submit"
          className="bg-indigo-600 mb-5 w-full  p-3 text-white uppercase font-bold hover:bg-indigo-700 cursor-pointer transition-colors"
          value={unPaciente.id ? "Editar Paciente " : "Agregar Paciente"}
        />
      </form>
    </div>
  );
};
