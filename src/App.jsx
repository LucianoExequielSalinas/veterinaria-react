import { useState, useEffect } from "react";

import { Formulario } from "./components/Formulario";
import Header from "./components/Header";
import { ListadoPacientes } from "./components/ListadoPacientes";
function App() {
  const [pacientes, setPacientes] = useState(
    JSON.parse(localStorage.getItem("pacientes")) ?? []
  );
  const [unPaciente, setUnPaciente] = useState([]);

  useEffect(() => {
    localStorage.setItem("pacientes", JSON.stringify(pacientes));
  }, [pacientes]);

  const elminarPaciente = (id) => {
    const pacientesActualizados = pacientes.filter(
      (paciente) => paciente.id !== id
    );
    setPacientes(pacientesActualizados);
  };

  return (
    <div className=" container mx-auto mt-20">
      <Header />
      <div className="mt-12 md:flex m">
        <Formulario
          pacientes={pacientes}
          setPacientes={setPacientes}
          unPaciente={unPaciente}
          setUnPaciente={setUnPaciente}
        />
        <ListadoPacientes
          pacientes={pacientes}
          setUnPaciente={setUnPaciente}
          elminarPaciente={elminarPaciente}
        />
      </div>
    </div>
  );
}

export default App;
